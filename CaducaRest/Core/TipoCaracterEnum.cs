﻿using System;
namespace CaducaRest.Core
{
    public enum TipoCaracterEnum
    {
        /// <summary>
        /// Se esta leyendo un indicador
        /// </summary>
        IndicadorInicio = 1,

        /// <summary>
        /// Se esta leyendo el tipo de valor
        /// </summary>
        ValorInicio = 2
    }
}
