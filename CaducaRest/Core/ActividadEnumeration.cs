﻿using System;
namespace CaducaRest.Core
{
    public enum ActividadEnumeration
    {
        Borrar      = -1,
        Insertar    = 1,
        Modficar    = 2,
        Autorizar   = 3,
        Cancelar    = 4 
    }
}
