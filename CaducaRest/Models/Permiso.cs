﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CaducaRest.Models
{
public class Permiso
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }

    [Required(ErrorMessage = "Required")]
    public int Clave { get; set; }

    [Column(TypeName = "VARCHAR(100)")]
    public string Nombre { get; set; }
}
}
